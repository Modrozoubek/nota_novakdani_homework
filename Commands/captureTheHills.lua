function getInfo()
	return {
		onNoUnits = FAIL, -- instant fail
		tooltip = "Move to defined positions",
		parameterDefs = {
			{ 
				name = "positions",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			}
		}
	}
end

-- speed-ups
local SpringGetUnitPosition = Spring.GetUnitPosition
local SpringGiveOrderToUnit = Spring.GiveOrderToUnit

local function ClearState(self)

end


function moveUnit(unit, position, cmdID) 
    local pointX, pointY, pointZ = SpringGetUnitPosition(unit)
    local unitPosition = Vec3(pointX, pointY, pointZ)
    if (unitPosition:Distance(position) < 25) then
        return true
    else
        SpringGiveOrderToUnit(unit, cmdID, position:AsSpringVector(), {})
        return false
    end
end


function Run(self, units, parameter)
	local positions = parameter.positions -- Vec3
	local fight = true -- boolean
	
	-- Spring.Echo(dump(parameter.position))
    
    if #units < #positions then
        return FAILURE
    end
	
	-- pick the spring command implementing the move
	local cmdID = CMD.MOVE
	if (fight) then cmdID = CMD.FIGHT end

    -- Move units
    finished = true
    for i=1,#units do
        if not moveUnit(units[i], positions[i], cmdID) then
            finished = false
        end        
    end
        
    if finished then return SUCCESS else return RUNNING end
end


function Reset(self)
	ClearState(self)
end
