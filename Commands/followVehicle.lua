function getInfo()
	return {
		onNoUnits = SUCCESS, -- instant success
		tooltip = "Move to defined position",
		parameterDefs = {
			{ 
				name = "position",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			},
			{ 
				name = "vehicleUnitId",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			}
		}
	}
end

-- constants
local THRESHOLD_STEP = 25
local THRESHOLD_DEFAULT = 0

-- speed-ups
local SpringGetUnitPosition = Spring.GetUnitPosition
local SpringGiveOrderToUnit = Spring.GiveOrderToUnit

local function ClearState(self)
	self.threshold = THRESHOLD_DEFAULT
	self.lastPointmanPosition = Vec3(0,0,0)
end

function Run(self, units, parameter)
	local position = parameter.position -- Vec3
	local vehicleUnitId = parameter.vehicleUnitId -- unitID
	local fight = true -- boolean
	
	-- Spring.Echo(dump(parameter.position))
    
    if not Spring.ValidUnitID(vehicleUnitId) then
        return FAILURE
    end
	
	-- pick the spring command implementing the move
	local cmdID = CMD.MOVE
	if (fight) then cmdID = CMD.FIGHT end

	local pointman = vehicleUnitId -- while this is running, we know that #units > 0, so pointman is valid
	local pointX, pointY, pointZ = SpringGetUnitPosition(pointman)
	local pointmanPosition = Vec3(pointX, pointY, pointZ)
	
	-- threshold of pointman success
	if (pointmanPosition == self.lastPointmanPosition) then 
		self.threshold = self.threshold + THRESHOLD_STEP 
	else
		self.threshold = THRESHOLD_DEFAULT
	end
	self.lastPointmanPosition = pointmanPosition
	
	-- check pointman success
	-- THIS LOGIC IS TEMPORARY, NOT CONSIDERING OTHER UNITS POSITION
	if (pointmanPosition:Distance(position) < self.threshold) then
		return SUCCESS
	else
		SpringGiveOrderToUnit(pointman, cmdID, position:AsSpringVector(), {})
		
		for i=1, #units do
            if units[i] ~= vehicleUnitId then
                local thisUnitWantedPosition = self.lastPointmanPosition -- pointmanPosition + formation[i]
                SpringGiveOrderToUnit(units[i], cmdID, thisUnitWantedPosition:AsSpringVector(), {})
            end
		end
		
		return RUNNING
	end
end


function Reset(self)
	ClearState(self)
end
