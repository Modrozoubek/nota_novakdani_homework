function getInfo()
	return {
		onNoUnits = SUCCESS, -- instant success
		tooltip = "Move to defined position",
		parameterDefs = {
			{ 
				name = "unitToRescueID",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			},
			{ 
				name = "unitTransportID",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			},
		}
	}
end


function Run(self, units, parameter)
    
    
    -- first time
    if not self.initialized then
        -- Transporter is not transporter
        if parameter.unitTransportID == nil or
            not UnitDefs[Spring.GetUnitDefID(parameter.unitTransportID)].isTransport then
            Logger.warn("Unit is not a transporter")
            return FAILURE
        end
        -- Unit to be transported is not transportable
        if parameter.unitToRescueID == nil or
            UnitDefs[Spring.GetUnitDefID(parameter.unitToRescueID)].cantBeTransported then
            Logger.warn("Unit is not transportable")
            return FAILURE
        end
    end
    
    
    -- Once running (always):
    -- FAILURE CONDITIONS
    -- Unit to be rescued is dead
    if not Spring.ValidUnitID(parameter.unitToRescueID) then
        Logger.warn("Unit is dead")
        return FAILURE
    end
    -- Transporter is dead
    if not Spring.ValidUnitID(parameter.unitTransportID) then
        Logger.warn("Transporter is dead")
        return FAILURE
    end
    -- They are far away from each other
    --local loadingRadius = UnitDefs[Spring.GetUnitDefID(parameter.unitTransportID)].loadingRadius
    --if Spring.GetUnitSeparation(parameter.unitToRescueID, parameter.unitTransportID) > loadingRadius then
    --    Logger.warn("Transporter is too far away from unit")
    --    return FAILURE
    --end
    -- Unit to be rescued is already in another transporter
    if Spring.GetUnitTransporter(parameter.unitToRescueID) ~= nil and 
       Spring.GetUnitTransporter(parameter.unitToRescueID) ~= parameter.unitTransportID then
        Logger.warn("Unit is in another transporter")
        return FAILURE
    end    

    
    -- SUCCESS CONDITIONS
    if Spring.GetUnitTransporter(parameter.unitToRescueID) == parameter.unitTransportID then
        return SUCCESS
    end
     
    
    -- first time
    if not self.initialized then
        -- Give order
        Spring.GiveOrderToUnit(parameter.unitTransportID, CMD.LOAD_UNITS,{parameter.unitToRescueID}, {})
        self.initialized = true
    end
    
    
    -- Unit stops to execute Spring order
    if #Spring.GetUnitCommands(parameter.unitTransportID) <= 0 then
        Spring.GiveOrderToUnit(parameter.unitTransportID, CMD.LOAD_UNITS,{parameter.unitToRescueID}, {})
    end
    
    
    
    return RUNNING
end

function Reset(self)
    self.initialized = false
end
