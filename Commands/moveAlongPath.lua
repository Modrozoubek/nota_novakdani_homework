function getInfo()
	return {
		onNoUnits = SUCCESS, -- instant success
		tooltip = "Move to defined position",
		parameterDefs = {
			{ 
				name = "unitID",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			},
            { 
				name = "targetID",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			},
		}
	}
end


local getDistance = function(a, b)
    local x, y, z = a.x-b.x, a.y-b.y, a.z-b.z
    return math.sqrt(x*x+y*y+z*z)
end


function Run(self, units, parameter)
    
    if parameter.targetID == nil then return FAILURE end
    local x,y,z = Spring.GetUnitPosition(parameter.targetID)
    local path = {Vec3(x,y,z)}
    
    -- local path = parameter.path
    
    -- first time
    if not self.initialized then
        -- no ID
        if parameter.unitID == nil then
            return FAILURE
        end
    end
    
    
    -- Once running (always):
    -- FAILURE CONDITIONS
    -- Unit is dead
    if not Spring.ValidUnitID(parameter.unitID) then
        return FAILURE
    end  
    
    
    -- SUCCESS CONDITIONS
    local x,y,z = Spring.GetUnitPosition(parameter.unitID)
    local distanceToTarget = getDistance(Vec3(x,y,z), path[#path])
    if distanceToTarget < 100 then
        return SUCCESS
    end
    
    
    -- first time
    if not self.initialized then
        -- Give orders
        for i = 1,#path do
            Spring.GiveOrderToUnit(parameter.unitID, CMD.MOVE, path[i]:AsSpringVector(), {"shift"})
        end
        self.initialized = true
    end
    
    return RUNNING
end

function Reset(self)
    self.initialized = false
end
