function getInfo()
	return {
		onNoUnits = SUCCESS, -- instant success
		tooltip = "Move to defined position",
		parameterDefs = {
			{ 
				name = "unitID",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			},
            { 
				name = "target",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			},
		}
	}
end


local mapSizeX = Game.mapSizeX
local mapSizeZ = Game.mapSizeZ


local isInMap = function(point)

    if point.x > 0 and point.x < mapSizeX and 
        point.z > 0 and point.z < mapSizeZ then
        return true
    else
        return false
    end
end


local getDistance = function(a, b)
    local x, z = a.x-b.x, a.z-b.z
    return math.sqrt(x*x+z*z)
end


local getDistanceToTarget = function(myID, target)
   local x1,y1,z1 = Spring.GetUnitPosition(myID)
   local a = Vec3(x1,y1,z1)

   return getDistance(a,target)
end


local getEnemies = Spring.GetTeamUnits(1)


local pointValue = function(point, target, currentDistance) 

    local direction = currentDistance - getDistance(point, target)
        
    value = direction * 10

    for i,enemy in pairs(getEnemies) do
        local x,y,z = Spring.GetUnitPosition(enemy)
        local enemyDistance = getDistance(point, Vec3(x,y,z))
        if enemyDistance < math.min(1000,currentDistance) then
            value = value - 1000
        end
        --value = value - 5000/enemyDistance
    end
    --value = value - Spring.GetGroundHeight(point.x, point.z)
    
    return value
end


local getNextPoint = function(unitID, target)

    local x,y,z = Spring.GetUnitPosition(unitID)
    local unitPos = Vec3(x,y,z)
    
    local currentDistance = getDistanceToTarget(unitID, target)
    local d = math.min(1000, currentDistance)
    
    local bestPoint = unitPos
    local bestValue = pointValue(bestPoint, target, currentDistance)
        
    for alfa = 0,2*math.pi,0.05*math.pi do
        local speed = math.min(500, currentDistance)
        local velocity = Vec3(speed * math.cos(alfa), 0, -math.sin(alfa) * speed)
        local point = unitPos + velocity
        if isInMap(point) then
            local value = pointValue(point, target, currentDistance)
            if bestValue < value then
                bestValue = value
                bestPoint = point
            end
        end
    end
    
    if bestPoint == unitPos then 
        Logger.warn("No travel point was chosen")
    end

    return bestPoint
end


function Run(self, units, parameter)
    
    local x,y,z = parameter.target[1], parameter.target[2], parameter.target[3]
    local target = Vec3(x,y,z)    

    -- first time
    if not self.initialized then
        -- no ID
        if parameter.unitID == nil then
            Logger.warn("No unitID parameter")
            return FAILURE
        end
        
        -- no target
        if target == nil then
            Logger.warn("No target parameter")
            return FAILURE
        end
        self.initialized = true
    end
    
    
    -- Once running (always):
    -- FAILURE CONDITIONS
    -- Unit is dead
    if not Spring.ValidUnitID(parameter.unitID) then
        Logger.warn("Move towards target: Unit is dead")
        return FAILURE
    end  
    
    
    -- SUCCESS CONDITIONS
    local distanceToTarget = getDistanceToTarget(parameter.unitID, target)
    if distanceToTarget < 100 then
        return SUCCESS
    end
    
    
    Spring.GiveOrderToUnit(parameter.unitID, CMD.MOVE, getNextPoint(parameter.unitID, target):AsSpringVector(), {})
    
    
    return RUNNING
end

function Reset(self)
    self.initialized = false
end
