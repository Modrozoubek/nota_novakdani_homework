function getInfo()
	return {
		onNoUnits = SUCCESS, -- instant success
		tooltip = "Move to defined position",
		parameterDefs = {
			{ 
				name = "unitTransportID",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			},
            { 
				name = "rescueArea",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			},
		}
	}
end


function Run(self, units, parameter)    
    
    -- first time
    if not self.initialized then
        -- Transporter is not transporter
        if parameter.unitTransportID == nil or
            not UnitDefs[Spring.GetUnitDefID(parameter.unitTransportID)].isTransport then
            Logger.warn("Unit is not transporter")
            return FAILURE
        end
    end
    
    
    -- Once running (always):
    -- FAILURE CONDITIONS
    -- Transporter is dead
    if not Spring.ValidUnitID(parameter.unitTransportID) then
        Logger.warn("Transporter is dead")
        return FAILURE
    end
    
    
    -- SUCCESS CONDITIONS
    if Spring.GetUnitIsTransporting(parameter.unitTransportID) == nil or 
           #Spring.GetUnitIsTransporting(parameter.unitTransportID) == 0 then
        return SUCCESS
    end
     
    
    -- first time
    if not self.initialized then
        -- Give order
        Spring.GiveOrderToUnit(parameter.unitTransportID, CMD.UNLOAD_UNITS,{parameter.rescueArea.center.x, parameter.rescueArea.center.y, parameter.rescueArea.center.z, parameter.rescueArea.radius}, {})
        self.initialized = true
    end
    
    
    -- Unit stops to execute Spring order
    if #Spring.GetUnitCommands(parameter.unitTransportID) <= 0 then
        --Spring.GiveOrderToUnit(parameter.unitTransportID, CMD.UNLOAD_UNITS,{parameter.rescueArea.center.x, parameter.rescueArea.center.y, parameter.rescueArea.center.z, parameter.rescueArea.radius}, {})
    end    
    
    
    return RUNNING
end

function Reset(self)
    self.initialized = false
end
