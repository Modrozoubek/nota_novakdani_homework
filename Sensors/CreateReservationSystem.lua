local sensorInfo = {
	name = "CreateReservationSystem",
	desc = "Makes reservation system.",
	author = "Modrozoubek",
	date = "2021-06-04",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1 -- no caching 

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end

local getDistance = function(a, b)
    local x, z = a.x-b.x, a.z-b.z
    return math.sqrt(x*x+z*z)
end

-- @description return reservation system
-- @argument atlases [array of unitIDs]  
-- @argument unitsToBeRescued [array of unitIDS] 
return function(atlases, unitsToBeRescued, safeArea)
	local reservationSystem = {
        atlases = {},
        unitsToBeRescued = {},
    }
	
	for i=1, #atlases do
		local atlasUnitID = atlases[i]
        reservationSystem.atlases[atlasUnitID] = "free"
	end
	
    for i=1, #unitsToBeRescued do
		local unitID = unitsToBeRescued[i]
        
        local x,y,z = Spring.GetUnitPosition(unitID)
        if getDistance(Vec3(x,y,z), safeArea.center) < safeArea.radius then
            reservationSystem.unitsToBeRescued[unitID] = "rescued"
        else
            reservationSystem.unitsToBeRescued[unitID] = "notRescued"
        end
	end
    
	return reservationSystem
end
