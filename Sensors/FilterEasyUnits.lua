local sensorInfo = {
	name = "FilterEasyUnits",
	desc = "Filter list of units so that are in the upper part of the map, therefore easy to rescue.",
	author = "Modrozoubek",
	date = "2021-06-04",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1 -- no caching 

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end

-- @description return filtered list of units
-- @argument listOfUnits [array of unitIDs] unfiltered list 
-- @return newListOfUnits [array of unitIDS] filtered list
return function(listOfUnits)
	local newListOfUnits = {}
	
	for i=1, #listOfUnits do
		local thisUnitID = listOfUnits[i]
        local x,y,z = Spring.GetUnitPosition(thisUnitID)
        if z < 6000 then -- is in the upper part of the map
            newListOfUnits[#newListOfUnits + 1] = thisUnitID
		end 
	end
	
	return newListOfUnits
end
