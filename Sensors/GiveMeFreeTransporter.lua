local sensorInfo = {
	name = "GiveMeFreeTransporter",
	desc = "Returns free transporter",
	author = "Modrozoubek",
	date = "2021-06-04",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1 -- no caching 

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end

-- @description return free transporter
-- @argument reservationSystemMemory [structure]
-- @returns free transporter
return function(reservationSystemMemory)
    local selectedUnitID        

	for unitID, state in pairs(reservationSystemMemory.atlases) do
        if state == "free" then
            selectedUnitID = unitID
            reservationSystemMemory.atlases[unitID] = "occupied"
            break
        end
	end
	
	return selectedUnitID
end
