local sensorInfo = {
	name = "GiveMeUnitToBeRescued",
	desc = "Returns free transporter",
	author = "Modrozoubek",
	date = "2021-06-04",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1 -- no caching 

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end

-- @description return free transporter
-- @argument reservationSystemMemory [structure]
-- @returns free transporter
return function(reservationSystemMemory)
    local selectedUnitID        

	for unitID, state in pairs(reservationSystemMemory.unitsToBeRescued) do
        if state == "notRescued" then
            selectedUnitID = unitID
            reservationSystemMemory.unitsToBeRescued[unitID] = "beingRescued"
            break
        end
	end
	
	return selectedUnitID
end
