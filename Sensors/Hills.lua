local sensorInfo = {
	name = "Hills",
	desc = "Return x,z positions of hills",
	author = "Modrozoubek",
	date = "2021-05-16",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = 100

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

-- speedups
local SpringGroundHeight = Spring.GetGroundHeight
local SpringGroundExtremes = Spring.GetGroundExtremes
local mapSizeX = Game.mapSizeX
local mapSizeZ = Game.mapSizeZ

-- @description return hill positions
return function()
    local minH, maxH = SpringGroundExtremes()
    
    local numberOfHills = 0
    
    local hills = {}
    
    for x=0,mapSizeX, 128 do
        for z=0,mapSizeZ, 128 do
            local height = Spring.GetGroundHeight(x,z)
            if height == maxH then 
                numberOfHills = numberOfHills + 1
                hills[numberOfHills] = Vec3(x,maxH,z)
            end
        end
    end
    
	return hills
	
end
