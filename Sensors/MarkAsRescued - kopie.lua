local sensorInfo = {
	name = "FindPathAStar",
	desc = "Finds path to target position",
	author = "Modrozoubek",
	date = "2021-06-04",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1 -- no caching 

local closedset = {}
local openset = {}    
local g_score, f_score = {}, {}

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end

function distance(from, to)
    local x,z = from.x-to.x, from.z-to.z
    return math.sqrt(x*x + z*z)
end

function heuristic(from, to) 
    return distance(from, to)
end

function lowest_f_score()
    --TODO
end

function return_path()
    --TODO
end
    
-- @description return free transporter
-- @argument reservationSystemMemory [structure]
-- @returns free transporter
return function(start, goal)
    
    g_score[start] = 0
    f_score[start] = g_score[start] + heuristic(start, goal)

    while #openset > 0 do
        
        local current = lowest_f_score()
        
        if current == goal then
            return return_path()
        end
        
    end
    return nil -- no valid path
end
