local sensorInfo = {
	name = "MarkAsRescued",
	desc = "Returns free transporter",
	author = "Modrozoubek",
	date = "2021-06-04",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1 -- no caching 

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end

-- @description return free transporter
-- @argument reservationSystemMemory [structure]
-- @returns free transporter
return function(reservationSystemMemory, transporterID, unitID)

    if transporterID ~= nil then
        reservationSystemMemory.atlases[transporterID] = "free"
    end
	if unitID ~= nil then
        reservationSystemMemory.unitsToBeRescued[unitID] = "rescued"
    end
	
	return reservationSystemMemory
end
