local sensorInfo = {
	name = "MarkAsToBeRescued",
	desc = "Returns free transporter",
	author = "Modrozoubek",
	date = "2021-06-04",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1 -- no caching 

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end

-- @description return free transporter
-- @argument reservationSystemMemory [structure]
-- @returns free transporter
return function(reservationSystemMemory, transporterID, unitID)

    if Spring.ValidUnitID(transporterID) then
        reservationSystemMemory.atlases[transporterID] = "free"
    else
        reservationSystemMemory.atlases[transporterID] = "dead"
    end
    
    if Spring.ValidUnitID(unitID) then
        reservationSystemMemory.unitsToBeRescued[unitID] = "notRescued"
    else 
        reservationSystemMemory.unitsToBeRescued[unitID] = "dead"
    end
	
	return reservationSystemMemory
end
