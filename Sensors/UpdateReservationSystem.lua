local sensorInfo = {
	name = "UpdateReservationSystem",
	desc = "Deletes missing units and checks if saved units are in safe safeArea.",
	author = "Modrozoubek",
	date = "2021-06-05",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1 -- no caching 

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end

local getDistance = function(a, b)
    local x, z = a.x-b.x, a.z-b.z
    return math.sqrt(x*x+z*z)
end

-- @description return reservation system
return function(reservationSystem, safeArea)

	for atlasUnitID, state in pairs(reservationSystem.atlases) do
        if not Spring.ValidUnitID(atlasUnitID) then
            reservationSystem.atlases[atlasUnitID] = "dead"
        end
	end
	
    for unitID, state in pairs(reservationSystem.unitsToBeRescued) do
        
        if not Spring.ValidUnitID(unitID) then
            reservationSystem.unitsToBeRescued[unitID] = "dead"
        end
        
        local x,y,z = Spring.GetUnitPosition(unitID)
        if getDistance(Vec3(x,y,z), safeArea.center) < safeArea.radius then
            reservationSystem.unitsToBeRescued[unitID] = "rescued"
        elseif reservationSystem.unitsToBeRescued[unitID] == "rescued" then
            reservationSystem.unitsToBeRescued[unitID] = "notRescued"
        end
	end
    
	return reservationSystem
end
